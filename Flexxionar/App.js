import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Image} from 'react-native';
import { Container, Content, Icon, Header, Body } from 'native-base'
import { createDrawerNavigator,  createAppContainer, createStackNavigator } from 'react-navigation'

import HomeScreen from './HomeScreen';
import SettingsScreen from './SettingsScreen';
import Parteexterna from './otraparte';

const MyApp = createDrawerNavigator({

  Home: { 
    screen: HomeScreen 
  },
  Settings: { 
    screen: SettingsScreen 
  },
  aBout: {
    screen: Parteexterna
  }, 
}, {
  navigationOptions: {
    headerStyle: {
      backgroundColor: 'red'
    }
  }
}
);


export default createAppContainer(MyApp);

const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  drawerHeader: {
    height: 200,
    backgroundColor: 'white'
  },
  drawerImage: {
    height: 150,
    width: 150,
    borderRadius: 75
  },
  	drawerLinkIcons: {
    height: 24,
    width: 24
  }, 
  drawerHeader: {
  	height: 170, 
  	backgroundColor: 'white'
  },
  icon: {
  	tintColor : '#F50057'
  }

})
