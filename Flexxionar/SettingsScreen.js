import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View, TextInput, TouchableOpacity, 
    ScrollView,
    Alert, } from 'react-native';

import {Icon, Button, Container, Header, Content, Left,  Right} from 'native-base';
import estilo from './android/estilo';


class SettingsScreen extends Component {
    state = {
        task: '',
        list: []
    };

    onPressAddtask = () => {
        if (this.state.task) {
            const newTask = this.state.task;
            const lastTask = this.state.list[0] || { id: 0 };
            const newid = Number(lastTask.id + 1);

            this.setState({
                list: [{id: newid, task: newTask}, ...this.state.list],
                task: ''
            });
        }
    }

    onPressDeleteTask = id => {
        Alert.alert('Eliminar', 'Va eliminar la tarea?', [
            {
                text: 'Si, eliminalo',
                onPress: () => {
                this.setState({
                    list: this.state.list.filter(task => task.id !== id)
                });
            }
        },
        {text:'No, no lo haga compa'}
    ]);
    }

  render() {
    const { list } = this.state;
    let zebraIndex = 1;
    return (
        <Container>
            <Header style={estilo.barritaInicial}>
            <Left>
            <Button transparent>
                <Icon ios='ios-menu' android="md-menu" style={{fontSize: 45}}
                onPress={() => this.props.navigation.openDrawer()} />
                </Button>
                </Left>
          <Right />
            </Header>
            <Content contentContainerStyle={{
            flex:1
        }}>
               <View style={estilo.contenido}>
            <ScrollView 
                contentContainerStyle={{flexGrow:1}}>
            <View style={estilo.list}>
                <View style={estilo.header}>
                    <Text style={estilo.headerText}>Todo List</Text>
                </View>
                
                <View style={estilo.add} >
                <TextInput style={estilo.inputText}
                placeholder="añadir una tarea"
                onChangeText={(value) => this.setState({ task: value })}
                value={this.state.task} />

                <TouchableOpacity style={estilo.button} onPress={this.onPressAddtask}>
                <Text style={estilo.submitText}>+ Agregar Tarea</Text>
                </TouchableOpacity>
                </View>

                {list.lenght === 0 && (
                    <View style={estilo.noTasks}>
                        <Text style={estilo.noTasksText}>There are no tasks yet, create a new one!</Text>
                    </View>
                )}

            {list.map((item, i) => {
                zebraIndex = zebraIndex === 2 ? 1 : 2;
                return (
                <View key={`task${i}`} style={estilo[`task${zebraIndex}`]}>
                <Text>{item.task}</Text>
                <TouchableOpacity onPress={() => {
                    this.onPressDeleteTask(item.id) }}>
                    <Text style={estilo.delete}>X
                    </Text>
                </TouchableOpacity>
                </View>);
            })}

            </View>
            </ScrollView>
            </View>
            </Content>
        </Container>
    );
  }
}


export default SettingsScreen;