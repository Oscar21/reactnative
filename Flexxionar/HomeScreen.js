import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import { createDrawerNavigator,  createAppContainer, SafeAreaView, DrawerItems } from 'react-navigation'
import {Icon, Button, Container, Header, Content, Left, Right} from 'native-base';

import estilo from './android/estilo';

class HomeScreen extends Component {
  render() {
    return (  
        <Container>
            <Header style={estilo.barritaInicial}>
                <Left>
                <Button transparent>
                <Icon ios='ios-menu' android="md-menu" style={{fontSize: 45}}
                onPress={() => this.props.navigation.openDrawer()} />
                </Button>
                </Left>
          <Right />
            </Header>
            <Content contentContainerStyle={{
            flex:1,
            alignItems: 'center',
            justifyContent: 'center',
        }} >
        
        <Image source={require('./Imagenes/config.png')} style={{height:100, width: 100}} />
                <Text>Home Screen</Text>
            </Content>
        </Container>
        
    );
  }
}

export default HomeScreen;