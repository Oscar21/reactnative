import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

//componente Home
import Todo from './Componente/Todo';

export default  class App extends Component {
  render() {
    return (
      <Todo />
    );
  }
}

//export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
