import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import estilo from './Estilo';

class Home extends Component {
    render() {
        return (
            <View style={estilo.contenido}>
                <View style={estilo.header}>
                    <Text style={estilo.headerText}>Header</Text>
                </View>
                <View style={estilo.columna}>
                    <View style={estilo.columna1}>
                        <Text style={estilo.columna1Text}>Columna1</Text>
                    </View>
                    <View style={estilo.columna2}>
                        <Text style={estilo.columna2Text}>Columna2</Text>
                    </View>
                    <View style={estilo.columna3}>
                        <Text style={estilo.columna3Text}>Columna3</Text>
                    </View>
                </View>
            </View>
        );
    }
}

export default Home;