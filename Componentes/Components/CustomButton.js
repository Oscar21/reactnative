import React from 'react';
import { Button, StyleSheet} from 'react-native';

const CustomButton = (props) => {
    return(
        <Button title="Agregar producto" color="#841584" 
        onPress={() => props.onPress()} />
    );
}

export default CustomButton;
