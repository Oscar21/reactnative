import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

import CustomButton from './Components/CustomButton';

export default class App extends React.Component {
  constructor(){
    super()
    this.state = {
      Contador: 0
    }
  }

  agregarProducto = () => {
    // this.setState({'Contador': this.state.Contador +1})
    // Otra forma de hacerlo
    this.setState((prevState) => ({'Contador': prevState.Contador + 1}))
  }

  render() {
    return (
      <View style={styles.container}>
        <CustomButton onPress={this.agregarProducto} style={styles.boton} />
        <Text>{this.state.Contador}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
