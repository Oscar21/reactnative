import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

import styles from './SectionStyles';

class Configuration extends Component {
    static navigationOptions = {
        drawerIcon: () => (
        <Image
        style={styles.iconsItem}
        source={require('../assets/config.png')}
        />
        )};
        
        render() {
            return(
            <View style={styles.container}>
            
            {/* Hamburger menu */}
            <TouchableOpacity
            onPress={() => this.props.navigation.openDrawer()}
            style={styles.iconMenu}>
            <Image
            style={styles.menu}/>
            </TouchableOpacity>
    
    {/* Here is the content of the component */}
    <Text style={styles.titleText}>I'm the configuration
    section</Text>
    </View>);}}
    
    export default Configuration;