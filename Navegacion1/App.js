import React, {Component} from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';

import {createDrawerNavigator, DrawerItems} from 'react-navigation'

import Home from './seccion/Home';
import Configuracion from './seccion/Configuracion';

const CustomDrawerComponent = props => (
  <View style={styles.area}>
    <View style={styles.drawer}>
      
    </View>
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </View>
);

const AppDrawerNavigator = createDrawerNavigator(
  {Home,
  Configuracion},
  {
    contentComponent: CustomDrawerComponent
  }
);

class App extends Component {
  render () {
    return(
      <AppDrawerNavigator/>
    );
  }
}

const styles = StyleSheet.create({
  area: {
    flex: 1
  },
  drawer: {
    height: 150,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent:'center'
  },
  logo: {
    height: 120,
    width: 120,
    borderRadius: 60
  }
});

export default App;