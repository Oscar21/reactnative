import React from 'react';
import { Dimensions, Platform} from 'react-native';

import { createDrawerNavigator, createAppContainer } from 'react-navigation';

import Home from './sections/Home';
import Configuration from './sections/Configuration';

const WIDTH = Dimensions.get('window').width;

const DrawerNavigator = createDrawerNavigator({
    Home: {
        screen: Home
    },
});

export default createAppContainer(DrawerNavigator);  
